module gitlab.com/logius/cloud-native-overheid/tools/environment-cli

go 1.15

require (
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sethvargo/go-password v0.2.0
	github.com/spf13/cobra v1.1.1
	github.com/xanzy/go-gitlab v0.42.0
	gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli v0.0.0-20210129195211-a34c76b744dc
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	gopkg.in/yaml.v2 v2.3.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	k8s.io/api v0.19.4
	k8s.io/apimachinery v0.19.4
	k8s.io/client-go v0.19.4
	k8s.io/utils v0.0.0-20200821003339-5e75c0163111 // indirect
	sigs.k8s.io/yaml v1.2.0
)
