package environment

import (
	"context"
	"log"
	"os"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

// ManageCerts manages the certificate requests for LetsEncrypt
func ManageCerts(k8sAPI *k8s.API, customerConfig *config.Customer, namespace config.Namespace) error {

	type CertParams struct {
		NamespaceName         string
		CustomerName          string
		CustomWildCardDNSName string
		ClusterIssuer         string
		ClusterDomain         string
	}

	params := CertParams{
		NamespaceName:         namespace.Name,
		CustomerName:          customerConfig.Name,
		CustomWildCardDNSName: namespace.CustomWildcardDNSName,
		ClusterIssuer:         os.Getenv("CERTMANAGER_ISSUER"),
		ClusterDomain:         os.Getenv("CERTMANAGER_CLUSTER_DOMAIN"),
	}

	gvr := schema.GroupVersionResource{
		Group:    "cert-manager.io",
		Version:  "v1",
		Resource: "certificates",
	}

	if namespace.SkipCertificates {
		log.Printf("Skip Certificates in namespace %s", namespace.Name)
		return nil
	}

	// Optionally provide extra wildcard_certificate per namespace in tenant-config
	if len(namespace.CustomWildcardDNSName) > 0 {
		var customerCert unstructured.Unstructured
		customCertFileName := "deployments/certs/custom_certificate.yml"
		if err := parseDeploymentFile(customCertFileName, params, &customerCert); err != nil {
			return err
		}
		if _, err := k8sAPI.Dynamic.Resource(gvr).Namespace(namespace.Name).Get(context.TODO(), customerCert.GetName(), metav1.GetOptions{}); err != nil {
			log.Printf("Create certificate %v in namespace %s", customerCert.GetName(), namespace.Name)
			_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(namespace.Name).Create(context.TODO(), &customerCert, metav1.CreateOptions{})
			if err != nil {
				return err
			}
		} else {
			// TODO, use Patch https://github.com/kubernetes-sigs/controller-runtime/blob/v0.8.3/pkg/client/interfaces.go#L38
			// log.Printf("Update certificate %v", customerCert.GetName())
			// _, err = k8s.Resource(gvr).Namespace(namespace.Name).Patch(context.TODO(), &customerCert, metav1.UpdateOptions{})
			// if err != nil {
			// 	log.Fatal(err) TODO
			// }
		}
	} else {
		log.Printf("No optional custom_wildcard_dns_name entry found for namespace %s", namespace.Name)
	}

	var cert unstructured.Unstructured
	fileName := "deployments/certs/certificate.yml"
	if err := parseDeploymentFile(fileName, params, &cert); err != nil {
		return err
	}

	if _, err := k8sAPI.Dynamic.Resource(gvr).Namespace(namespace.Name).Get(context.TODO(), cert.GetName(), metav1.GetOptions{}); err != nil {
		log.Printf("Create certificate %v in namespace %s", cert.GetName(), namespace.Name)
		_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(namespace.Name).Create(context.TODO(), &cert, metav1.CreateOptions{})
		if err != nil {
			return err
		}
	} else {
		// TODO
		// log.Printf("Update certificate %v", cert.GetName())
		// _, err = k8s.Resource(gvr).Namespace(namespace.Name).Update(context.TODO(), &cert, metav1.UpdateOptions{})
		// if err != nil {
		// 	log.Fatal(err) TODO
		// }
	}
	return nil
}
