package environment

import (
	"sync"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"
)

type flags struct {
	config                      *string
	context                     *string
	stage                       *string
	gitlabURL                   *string
	postfixEnvironmentScope     *string
	skipPostgresConfiguration   *bool
	groupPrefix                 *string
	gitlabIntegration           *bool
	forceRecreateServiceAccount *bool
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := configureEnvironment(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "configure-environment",
		Short: "Configure K8s environment",
		Long:  "This command configures namespaces, RBAC, certificates, policies and limits for a tenant in a K8s cluster.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.context = cmd.Flags().String("context", "", "K8s context")
	flags.gitlabURL = cmd.Flags().String("gitlab_url", "", "URL of GitLab")
	flags.stage = cmd.Flags().String("stage", "", "Stage, e.g. OT, AP")
	flags.groupPrefix = cmd.Flags().String("group-prefix", "/", "Prefix for rbac group name")
	flags.postfixEnvironmentScope = cmd.Flags().String("postfix-environment-scope", "", "Postfix for environment scope in Gitlab Kubernetes integration")
	flags.skipPostgresConfiguration = cmd.Flags().Bool("skip-postgres-configuration", false, "Default false. When 'true' skip any postgres configuration in this run: no changes regarding Postgres configuration are made")
	flags.gitlabIntegration = cmd.Flags().Bool("kubernetes-gitlab-integration", true, "Default true. When 'false' skip kubernetes integration for cluster in gitlab groups.")
	flags.forceRecreateServiceAccount = cmd.Flags().Bool("force-recreate-service-account", true, "Default false. When 'true' the deployer service account(s) in the admin namespace is deleted and recreated.")
	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("gitlab_url")
	cmd.MarkFlagRequired("stage")

	return cmd
}

// Run configures the environments
func configureEnvironment(cmd *cobra.Command, flags *flags) error {

	customerConfig, err := config.LoadConfigfile(*flags.config)
	if err != nil {
		return err
	}
	environment, err := customerConfig.GetEnvironment(*flags.stage)
	if err != nil {
		return err
	}

	// K8s API clients
	k8sAPI, err := k8s.CreateAPI(*flags.context)
	if err != nil {
		return err
	}

	// GitLab API client
	gitlabClient := gitlabclient.NewGitLabClient(*flags.gitlabURL)

	// Create impersonator clusterrole
	err = manageCustomerClusterRole(k8sAPI, customerConfig, *environment, *flags.groupPrefix)
	if err != nil {
		return err
	}

	// Use WaitGroup to handle namespaces in parallel. See https://golangcode.com/errors-in-waitgroups/ for error handling
	var wg sync.WaitGroup
	errorChannel := make(chan error)
	wgDone := make(chan bool)

	for _, ns := range environment.Namespaces {

		wg.Add(1)
		go func(environment config.Environment, ns config.Namespace) {
			defer wg.Done()
			if err := ManageNamespace(k8sAPI, customerConfig, environment, ns, *flags.skipPostgresConfiguration); err != nil {
				errorChannel <- err
			}
			if err := ManageRBAC(k8sAPI, gitlabClient, customerConfig, environment, ns, *flags.postfixEnvironmentScope, *flags.groupPrefix, *flags.gitlabIntegration, *flags.forceRecreateServiceAccount); err != nil {
				errorChannel <- err
			}
			if err := ManageResourceQuota(k8sAPI, customerConfig, ns); err != nil {
				errorChannel <- err
			}
			if err := ManageLimits(k8sAPI, customerConfig, ns); err != nil {
				errorChannel <- err
			}
			if err := ManagePolicies(k8sAPI, ns, *flags.skipPostgresConfiguration); err != nil {
				errorChannel <- err
			}
			if err := ManageCerts(k8sAPI, customerConfig, ns); err != nil {
				errorChannel <- err
			}
			if !*flags.skipPostgresConfiguration {
				if err := ManagePostgres(k8sAPI, customerConfig, ns); err != nil {
					errorChannel <- err
				}
			}
		}(*environment, ns)
	}

	go func() {
		wg.Wait()
		close(wgDone)
	}()

	// Wait until either WaitGroup is done or an error is received through the channel
	select {
	case <-wgDone:
		// carry on
		break
	case err := <-errorChannel:
		close(errorChannel)
		return err
	}

	return nil
}
