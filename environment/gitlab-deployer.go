package environment

import (
	"context"
	"github.com/pkg/errors"
	"log"
	"strings"
	"time"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"

	"github.com/xanzy/go-gitlab"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// SaveGitLabServiceAccounts saves the token for the deployer service account in GitLab
func SaveGitLabServiceAccounts(k8sAPI *k8s.API, gitlabClient *gitlab.Client, gitlabConfig config.GitLab,
	serviceAccount *v1.ServiceAccount, postfixEnvironmentScope string) error {

	deployerToken, err := waitForDeployerToken(k8sAPI, serviceAccount)
	if err != nil {
		return err
	}

	for _, gitlabGroupConfig := range gitlabConfig.Groups {
		gitlabGroup := gitlabclient.GetGitLabGroup(gitlabClient, gitlabGroupConfig.Name)
		var environmentScope = k8sAPI.Cluster + postfixEnvironmentScope
		if err := saveGitLabServiceAccountInGroup(k8sAPI, gitlabClient, gitlabGroup, deployerToken, environmentScope); err != nil {
			return err
		}
	}
	return nil
}

func saveGitLabServiceAccountInGroup(k8sAPI *k8s.API, gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group, deployerToken string, environmentScope string) error {

	apiCertificate := string(k8sAPI.Config.CertData)
	apiServerURL := k8sAPI.Config.Host + k8sAPI.Config.APIPath
	groupCluster := getGroupCluster(k8sAPI, gitlabClient, gitlabGroup)

	if groupCluster != nil {
		editOptions := gitlab.EditGroupClusterOptions{
			Name:             &k8sAPI.Cluster,
			EnvironmentScope: &environmentScope,
			PlatformKubernetes: &gitlab.EditGroupPlatformKubernetesOptions{
				APIURL: &apiServerURL,
				Token:  &deployerToken,
				CaCert: &apiCertificate,
			},
		}
		log.Printf("Update cluster %q in group %q", k8sAPI.Cluster, gitlabGroup.FullPath)
		_, _, err := gitlabClient.GroupCluster.EditCluster(gitlabGroup.ID, groupCluster.ID, &editOptions)
		if err != nil {
			return err
		}
		return nil
	}

	managed := false
	addOptions := gitlab.AddGroupClusterOptions{
		Name:             &k8sAPI.Cluster,
		EnvironmentScope: &k8sAPI.Cluster,
		Managed:          &managed,
		PlatformKubernetes: &gitlab.AddGroupPlatformKubernetesOptions{
			APIURL: &apiServerURL,
			Token:  &deployerToken,
			CaCert: &apiCertificate,
		},
	}
	log.Printf("Add cluster %q to group %q", k8sAPI.Cluster, gitlabGroup.FullPath)
	_, _, err := gitlabClient.GroupCluster.AddCluster(gitlabGroup.ID, &addOptions)
	if err != nil {
		return err
	}
	return nil
}

func getGroupCluster(k8sAPI *k8s.API, gitlabClient *gitlab.Client, gitlabGroup *gitlab.Group) *gitlab.GroupCluster {

	groupClusters, _, _ := gitlabClient.GroupCluster.ListClusters(gitlabGroup.ID)
	for _, groupCluster := range groupClusters {
		if groupCluster.Name == k8sAPI.Cluster {
			return groupCluster
		}
	}

	return nil
}

// deployer token is added by K8s to the service account
// waitForDeployerToken loops until the token is available
func waitForDeployerToken(k8sAPI *k8s.API, serviceAccount *v1.ServiceAccount) (string, error) {

	for i := 0; i < 10; i++ {
		token, err := getDeployerToken(k8sAPI, serviceAccount)
		if err != nil {
			return "", err
		}
		if token != "" {
			return token, nil
		}
		time.Sleep(500 * time.Millisecond)
	}
	return "", errors.Errorf("Could not find deployer token for serviceaccount %q in namespace %q", serviceAccount.Name, serviceAccount.Namespace)
}

func getDeployerToken(k8sAPI *k8s.API, serviceAccount *v1.ServiceAccount) (string, error) {

	serviceAccount, err := k8sAPI.Clientset.CoreV1().ServiceAccounts(serviceAccount.Namespace).Get(context.TODO(), serviceAccount.Name, metav1.GetOptions{})
	if err != nil {
		return "", err
	}

	// find the secret with type 'SecretTypeServiceAccountToken' and return its token.
	for _, secretReference := range serviceAccount.Secrets {
		secret, err := k8sAPI.Clientset.CoreV1().Secrets(serviceAccount.Namespace).Get(context.TODO(), secretReference.Name, metav1.GetOptions{})
		if err != nil {
			return "", err
		}
		if secret.Type == v1.SecretTypeServiceAccountToken {
			return string(secret.Data["token"]), nil
		}
	}

	// If not found in ServiceAccount reference for secrets
	// Try find the secret with type 'SecretTypeServiceAccountToken' in list of secrets
	listOpts := metav1.ListOptions{}
	listOpts.FieldSelector = "type=" + string(v1.SecretTypeServiceAccountToken)
	secrets, err := k8sAPI.Clientset.CoreV1().Secrets(serviceAccount.Namespace).List(context.TODO(), listOpts)
	if err != nil {
		return "", err
	}
	for _, secret := range secrets.Items {
		// Check secret name <serviceAccount.Name>-token-
		prefix := serviceAccount.Name + "-token-"
		if strings.HasPrefix(secret.Name, prefix) {
			log.Printf("Found secret %q:", secret.Name)
			return string(secret.Data["token"]), nil
		}
	}
	
	return "", nil
}
