package environment

import (
	"bytes"
	"context"
	"html/template"
	"log"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"

	"github.com/xanzy/go-gitlab"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	yaml "sigs.k8s.io/yaml"
)

// RBACManager manages the RBAC in an environment
type RBACManager struct {
	// rolebindings administers role bindings created by RBACManager in function createRoleBinding
	rolebindings []string
	// serviceAccounts administers service accounts created by RBACManager in function createServiceAccount
	serviceAccounts []string
}

// ManageRBAC creates rolebindings and service accounts in the namespace
func ManageRBAC(k8sAPI *k8s.API, gitlabClient *gitlab.Client, customerConfig *config.Customer,
	environment config.Environment, namespace config.Namespace, postfixEnvironmentScope string, groupPrefix string, gitlabIntegration bool, forceRecreateServiceAccount bool) error {

	// RBACManager to keep track of created objects in the namespace.
	rbacManager := &RBACManager{}

	if err := rbacManager.manageRoleBindings(k8sAPI, namespace, groupPrefix); err != nil {
		return err
	}
	if err := rbacManager.manageServiceAccounts(k8sAPI, gitlabClient, customerConfig, environment, namespace, postfixEnvironmentScope, gitlabIntegration, forceRecreateServiceAccount); err != nil {
		return err
	}
	if err := rbacManager.manageSecrets(k8sAPI, gitlabClient, customerConfig, environment, namespace); err != nil {
		return err
	}
	if err := rbacManager.removeRedunantRoleBindings(k8sAPI, namespace); err != nil {
		return err
	}
	if err := rbacManager.removeRedundantServiceAccounts(k8sAPI, namespace); err != nil {
		return err
	}

	return nil
}

// manageRoleBindings creates or updates rolebindings for users (i.e. impersonator) and groups (i.e. keycloak groups)
// both configurations can have a single item or a list
func (rbacManager *RBACManager) manageRoleBindings(k8sAPI *k8s.API, namespace config.Namespace, groupPrefix string) error {

	for _, groupRolebinding := range namespace.Rolebindings {

		for _, group := range groupRolebinding.Groups {
			if err := rbacManager.manageGroupRoleBinding(k8sAPI, namespace, groupRolebinding.ClusterRole, groupRolebinding.Kind, group, groupPrefix); err != nil {
				return err
			}
		}

		if groupRolebinding.Group != "" {
			if err := rbacManager.manageGroupRoleBinding(k8sAPI, namespace, groupRolebinding.ClusterRole, groupRolebinding.Kind, groupRolebinding.Group, groupPrefix); err != nil {
				return err
			}
		}
	}
	for _, userRolebinding := range namespace.ImpersonateRolebindings {

		for _, user := range userRolebinding.Users {
			if err := rbacManager.manageUserRoleBinding(k8sAPI, namespace, userRolebinding.ClusterRole, userRolebinding.Kind, user); err != nil {
				return err
			}
		}

		if userRolebinding.User != "" {
			if err := rbacManager.manageUserRoleBinding(k8sAPI, namespace, userRolebinding.ClusterRole, userRolebinding.Kind, userRolebinding.User); err != nil {
				return err
			}
		}
	}
	return nil
}

func (rbacManager *RBACManager) removeRedunantRoleBindings(k8sAPI *k8s.API, namespace config.Namespace) error {
	roleBindingList, _ := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).List(context.TODO(), metav1.ListOptions{})
	for _, rb := range roleBindingList.Items {
		if isManagedByCNO(rb.ObjectMeta) && !stringInSlice(rb.Name, rbacManager.rolebindings) {
			log.Printf("Delete rolebinding %s from namespace %s", rb.Name, namespace.Name)
			if err := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Delete(context.TODO(), rb.Name, metav1.DeleteOptions{}); err != nil {
				return err
			}
		}
	}
	return nil
}

func (rbacManager *RBACManager) removeRedundantServiceAccounts(k8sAPI *k8s.API, namespace config.Namespace) error {
	serviceAccounts, _ := k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).List(context.TODO(), metav1.ListOptions{})
	for _, sa := range serviceAccounts.Items {
		if isManagedByCNO(sa.ObjectMeta) && !stringInSlice(sa.Name, rbacManager.serviceAccounts) {
			log.Printf("Delete serviceaccount %s from namespace %s", sa.Name, namespace.Name)
			if err := k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).Delete(context.TODO(), sa.Name, metav1.DeleteOptions{}); err != nil {
				return err
			}
		}
	}
	return nil
}

func (rbacManager *RBACManager) manageGroupRoleBinding(k8sAPI *k8s.API, namespace config.Namespace, clusterRole string, roleKind string, groupName string, groupPrefix string) error {

	type RoleBindingParams struct {
		ClusterRole string
		GroupName   string
		GroupPrefix string
	}

	params := RoleBindingParams{
		ClusterRole: clusterRole,
		GroupName:   groupName,
		GroupPrefix: groupPrefix,
	}

	var rolebinding rbacv1.RoleBinding
	parseDeploymentFile("deployments/rolebindings/group-rolebinding.yml", params, &rolebinding)
	if err := rbacManager.createRoleBinding(k8sAPI, rolebinding, namespace, "group '"+groupName+"'"); err != nil {
		return err
	}
	return nil
}

func (rbacManager *RBACManager) manageUserRoleBinding(k8sAPI *k8s.API, namespace config.Namespace, clusterRole string, roleKind string, userName string) error {

	type RoleBindingParams struct {
		ClusterRole string
		UserName    string
	}

	params := RoleBindingParams{
		ClusterRole: clusterRole,
		UserName:    userName,
	}

	var rolebinding rbacv1.RoleBinding
	parseDeploymentFile("deployments/rolebindings/user-rolebinding.yml", params, &rolebinding)
	if err := rbacManager.createRoleBinding(k8sAPI, rolebinding, namespace, "user '"+userName+"'"); err != nil {
		return err
	}
	return nil
}

// manageSecrets creates or updates secrets
func (rbacManager *RBACManager) manageSecrets(k8sAPI *k8s.API, gitlabClient *gitlab.Client, customerConfig *config.Customer, environment config.Environment, namespace config.Namespace) error {
	if err := SaveHarborPuller(k8sAPI, gitlabClient, customerConfig, environment, namespace); err != nil {
		return err
	}
	return nil
}

// manageServiceAccounts creates or updates service account for the deployer role and optionally custom service accounts as specified in config
func (rbacManager *RBACManager) manageServiceAccounts(k8sAPI *k8s.API, gitlabClient *gitlab.Client,
	customerConfig *config.Customer, environment config.Environment, namespace config.Namespace, postfixEnvironmentScope string, gitlabIntegration bool, forceRecreate bool) error {

	var serviceAccount corev1.ServiceAccount
	var createdServiceAccount *corev1.ServiceAccount
	var err error

	type DeployerServiceAccountParams struct {
		NamespaceName      string
		CustomerName       string
		AdminNamespaceName string
		ClusterRole        string
	}
	deployerParams := DeployerServiceAccountParams{
		NamespaceName:      namespace.Name,
		CustomerName:       customerConfig.Name,
		AdminNamespaceName: environment.AdminNamespace,
		ClusterRole:        environment.DeployerSaClusterRole,
	}

	// Deployer service account only in admin namespace, also add to GitLab
	if namespace.Name == environment.AdminNamespace {
		if err := parseDeploymentFile("deployments/serviceaccounts/deployer.yml", deployerParams, &serviceAccount); err != nil {
			return err
		}
		if createdServiceAccount, err = rbacManager.createServiceAccount(k8sAPI, serviceAccount, namespace, forceRecreate); err != nil {
			return err
		}
		if gitlabIntegration {
			if err := SaveGitLabServiceAccounts(k8sAPI, gitlabClient, customerConfig.GitLab, createdServiceAccount, postfixEnvironmentScope); err != nil {
				return err
			}
		}
	}

	// Rolebindings for deployer service account in all namespaces
	var roleBinding rbacv1.RoleBinding
	if err := parseDeploymentFile("deployments/serviceaccounts/deployer-rolebinding.yml", deployerParams, &roleBinding); err != nil {
		return err
	}
	if err := rbacManager.createRoleBinding(k8sAPI, roleBinding, namespace, "deployer account"); err != nil {
		return err
	}

	// Custom service accounts as configured for namespace
	type CustomServiceAccountParams struct {
		NamespaceName      string
		ServiceAccountName string
		ClusterRole        string
	}

	for _, customServiceAccount := range namespace.ServiceAccounts {
		customParams := CustomServiceAccountParams{
			NamespaceName:      namespace.Name,
			ServiceAccountName: customServiceAccount.Name,
			ClusterRole:        customServiceAccount.ClusterRole,
		}
		// Create service account
		if err := parseDeploymentFile("deployments/serviceaccounts/custom-sa.yml", customParams, &serviceAccount); err != nil {
			return err
		}
		if _, err = rbacManager.createServiceAccount(k8sAPI, serviceAccount, namespace, forceRecreate); err != nil {
			return err
		}

		// Create rolebinding
		if err := parseDeploymentFile("deployments/serviceaccounts/custom-sa-rolebinding.yml", customParams, &roleBinding); err != nil {
			return err
		}
		if err := rbacManager.createRoleBinding(k8sAPI, roleBinding, namespace, "service account '"+customServiceAccount.Name+"'"); err != nil {
			return err
		}
	}

	return nil
}

// createServiceAccount creates or updates the specified service account
func (rbacManager *RBACManager) createServiceAccount(k8sAPI *k8s.API, serviceAccount corev1.ServiceAccount, namespace config.Namespace, force bool) (*corev1.ServiceAccount, error) {
	serviceAccount.ObjectMeta.Labels = commonLabels(serviceAccount.ObjectMeta.Labels)
	var createdServiceAccount *corev1.ServiceAccount

	if orgServiceAccount, err := k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).Get(context.TODO(), serviceAccount.Name, metav1.GetOptions{}); err != nil {
		log.Printf("Create serviceaccount %v in namespace %s", serviceAccount.Name, namespace.Name)
		createdServiceAccount, err = k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).Create(context.TODO(), &serviceAccount, metav1.CreateOptions{})
		if err != nil {
			return nil, err
		}
	} else if force {
		log.Printf("Recreate serviceaccount %v in namespace %s (force=true)", serviceAccount.Name, namespace.Name)
		err = k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).Delete(context.TODO(), serviceAccount.Name, metav1.DeleteOptions{})
		if err != nil {
			log.Printf("Error deleting old serviceaccount %v in namespace %s", serviceAccount.Name, namespace.Name)
			return nil, err
		}
		createdServiceAccount, err = k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).Create(context.TODO(), &serviceAccount, metav1.CreateOptions{})
		if err != nil {
			return nil, err
		}
	} else {
		log.Printf("Update serviceaccount %v in namespace %s", serviceAccount.Name, namespace.Name)
		orgServiceAccount.ObjectMeta.Labels = commonLabels(serviceAccount.ObjectMeta.Labels)
		createdServiceAccount, err = k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).Update(context.TODO(), orgServiceAccount, metav1.UpdateOptions{})
		if err != nil {
			return nil, err
		}
	}

	rbacManager.serviceAccounts = append(rbacManager.serviceAccounts, serviceAccount.Name)
	return createdServiceAccount, nil
}

// createRoleBinding creates or updates the specified role binding
// subjectDescription parameter is for log message
func (rbacManager *RBACManager) createRoleBinding(k8sAPI *k8s.API, roleBinding rbacv1.RoleBinding, namespace config.Namespace, subjectDescription string) error {

	// Set 'managed by cno' label
	roleBinding.ObjectMeta.Labels = commonLabels(roleBinding.ObjectMeta.Labels)

	if _, err := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Get(context.TODO(), roleBinding.Name, metav1.GetOptions{}); err != nil {
		log.Printf("Create rolebinding %s for binding %s to %s in namespace %s", roleBinding.Name, subjectDescription, roleBinding.RoleRef.Name, namespace.Name)
		_, err = k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Create(context.TODO(), &roleBinding, metav1.CreateOptions{})
		if err != nil {
			return err
		}
	} else {
		// Update of role in the rolebinding is not (always) possible, so try and if not possible, delete and recreate
		log.Printf("Update rolebinding %s for binding %s to %s in namespace %s", roleBinding.Name, subjectDescription, roleBinding.RoleRef.Name, namespace.Name)
		_, err = k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Update(context.TODO(), &roleBinding, metav1.UpdateOptions{})
		if err != nil {

			log.Printf("Update not possible, so re-create rolebinding %s for binding  %s to %s in namespace %s", roleBinding.Name, subjectDescription, roleBinding.RoleRef.Name, namespace.Name)
			err = k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Delete(context.TODO(), roleBinding.Name, metav1.DeleteOptions{})
			if err != nil {
				return err
			}
			_, err = k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Create(context.TODO(), &roleBinding, metav1.CreateOptions{})
			if err != nil {
				return err
			}

		}
	}

	rbacManager.rolebindings = append(rbacManager.rolebindings, roleBinding.Name)
	return nil
}

func parseDeploymentFile(fileName string, variables interface{}, object interface{}) error {
	tpl, err := template.ParseFiles(fileName)
	if err != nil {
		return err
	}

	var buf bytes.Buffer
	err = tpl.Execute(&buf, variables)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(buf.Bytes(), object)
	if err != nil {
		return err
	}
	return nil
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func appendIfMissing(slice []string, item string) []string {
	for _, ele := range slice {
		if ele == item {
			return slice
		}
	}
	return append(slice, item)
}

const managedByLabel = "app.kubernetes.io/managed-by"

func isManagedByCNO(metadata metav1.ObjectMeta) bool {
	if metadata.Labels == nil {
		return false
	}
	for key, value := range metadata.Labels {
		if key == managedByLabel && value == "cno" {
			return true
		}
	}
	return false
}

func commonLabels(labels map[string]string) map[string]string {
	if labels == nil {
		labels = make(map[string]string)
	}
	labels[managedByLabel] = "cno"

	return labels
}
