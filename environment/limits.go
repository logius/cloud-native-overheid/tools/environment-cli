package environment

import (
	"context"
	"log"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ManageLimits manages the LimitRanges manifest
func ManageLimits(k8sAPI *k8s.API, customerConfig *config.Customer, namespace config.Namespace) error {
	fileName := "deployments/limitrange/default-limits.yml"
	var limits corev1.LimitRange
	err := parseDeploymentFile(fileName, nil, &limits)
	if err != nil {
		return err
	}

	if _, err := k8sAPI.Clientset.CoreV1().LimitRanges(namespace.Name).Get(context.TODO(), limits.Name, metav1.GetOptions{}); err != nil {
		log.Printf("Create limitrange %v in namespace %s", limits.Name, namespace.Name)
		_, err = k8sAPI.Clientset.CoreV1().LimitRanges(namespace.Name).Create(context.TODO(), &limits, metav1.CreateOptions{})
		if err != nil {
			return err
		}
	} else {
		log.Printf("Update limitrange %v in namespace %s", limits.Name, namespace.Name)
		_, err = k8sAPI.Clientset.CoreV1().LimitRanges(namespace.Name).Update(context.TODO(), &limits, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}

// ManageResourceQuota manages the resourcequota in the namespace
func ManageResourceQuota(k8sAPI *k8s.API, customerConfig *config.Customer, namespace config.Namespace) error {

	type ResourceQuotaParams struct {
		NamespaceName string
		CustomerName  string
		ResourceQuota config.ResourceQuota
	}

	params := ResourceQuotaParams{
		NamespaceName: namespace.Name,
		CustomerName:  customerConfig.Name,
		ResourceQuota: namespace.ResourceQuota,
	}

	fileName := "deployments/resourcequota/resourcequota.yml"
	setResourceQuotaDefaults(&params.ResourceQuota)
	if namespace.NoResourceQuota {
		fileName = "deployments/resourcequota/noresourcequota.yml"
	}

	var resourceQuota corev1.ResourceQuota
	err := parseDeploymentFile(fileName, params, &resourceQuota)
	if err != nil {
		return err
	}

	if _, err := k8sAPI.Clientset.CoreV1().ResourceQuotas(namespace.Name).Get(context.TODO(), resourceQuota.Name, metav1.GetOptions{}); err != nil {
		log.Printf("Create resourceQuota %v in namespace %s", resourceQuota.Name, namespace.Name)
		_, err = k8sAPI.Clientset.CoreV1().ResourceQuotas(namespace.Name).Create(context.TODO(), &resourceQuota, metav1.CreateOptions{})
		if err != nil {
			return err
		}
	} else {
		log.Printf("Update resourceQuota %v in namespace %s", resourceQuota.Name, namespace.Name)
		_, err = k8sAPI.Clientset.CoreV1().ResourceQuotas(namespace.Name).Update(context.TODO(), &resourceQuota, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}

func setResourceQuotaDefaults(resourceQuota *config.ResourceQuota) {
	if resourceQuota.CPULimits == "" {
		resourceQuota.CPULimits = "0m"
	}
	if resourceQuota.MemoryLimits == "" {
		resourceQuota.MemoryLimits = "0G"
	}
	if resourceQuota.CPURequests == "" {
		resourceQuota.CPURequests = "0m"
	}
	if resourceQuota.MemoryRequests == "" {
		resourceQuota.MemoryRequests = "0G"
	}
	if resourceQuota.StorageRequests == "" {
		resourceQuota.StorageRequests = "0G"
	}

}
