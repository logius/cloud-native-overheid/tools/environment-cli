package environment

import (
	"context"
	"log"
	"path/filepath"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"

	corev1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ManagePolicies manages the network policies
func ManagePolicies(k8sAPI *k8s.API, namespace config.Namespace, skipPostgresPart bool) error {
	if !namespace.DisableDefaultNetworkPolicies {

		policyFiles, _ := filepath.Glob("deployments/networkpolicies/*.yml")
		for _, fileName := range policyFiles {
			if err := applyPolicy(k8sAPI, namespace, fileName); err != nil {
				return err
			}
		}

		if skipPostgresPart {
			log.Printf("Skipping Postgres configuration")
			return nil
		}
		if namespace.Postgres {
			log.Printf("Postgres policies required in namespace %s", namespace.Name)
			policyFiles, _ := filepath.Glob("deployments/networkpolicies/postgres/*.yml")
			for _, fileName := range policyFiles {
				if err := applyPolicy(k8sAPI, namespace, fileName); err != nil {
					return err
				}
			}
		} else {
			log.Printf("No Postgres policies required in namespace %s", namespace.Name)
			policyFiles, _ := filepath.Glob("deployments/networkpolicies/postgres/*.yml")
			for _, fileName := range policyFiles {
				if err := deletePolicy(k8sAPI, namespace, fileName); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func applyPolicy(k8sAPI *k8s.API, namespace config.Namespace, fileName string) error {
	var policy corev1.NetworkPolicy
	if err := parseDeploymentFile(fileName, nil, &policy); err != nil {
		return err
	}

	if _, err := k8sAPI.Clientset.NetworkingV1().NetworkPolicies(namespace.Name).Get(context.TODO(), policy.Name, metav1.GetOptions{}); err != nil {
		log.Printf("Create policy %v in namespace %s", policy.Name, namespace.Name)
		if _, err = k8sAPI.Clientset.NetworkingV1().NetworkPolicies(namespace.Name).Create(context.TODO(), &policy, metav1.CreateOptions{}); err != nil {
			return err
		}
	} else {
		log.Printf("Update policy %v in namespace %s", policy.Name, namespace.Name)
		if _, err = k8sAPI.Clientset.NetworkingV1().NetworkPolicies(namespace.Name).Update(context.TODO(), &policy, metav1.UpdateOptions{}); err != nil {
			return err
		}
	}
	return nil
}

func deletePolicy(k8sAPI *k8s.API, namespace config.Namespace, fileName string) error {
	var policy corev1.NetworkPolicy
	if err := parseDeploymentFile(fileName, nil, &policy); err != nil {
		return err
	}

	if _, err := k8sAPI.Clientset.NetworkingV1().NetworkPolicies(namespace.Name).Get(context.TODO(), policy.Name, metav1.GetOptions{}); err != nil {
		// Policy not present, continue
	} else {
		log.Printf("Delete policy %v in namespace %s", policy.Name, namespace.Name)
		if err = k8sAPI.Clientset.NetworkingV1().NetworkPolicies(namespace.Name).Delete(context.TODO(), policy.Name, metav1.DeleteOptions{}); err != nil {
			return err
		}
	}
	return nil
}
