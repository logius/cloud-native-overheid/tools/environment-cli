package environment

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"github.com/sethvargo/go-password/password"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"
	"gopkg.in/yaml.v2"
	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Content types
const jsonContentType = "application/json"

// ManagePostgres creates rolebindings and service accounts in the namespace
func ManagePostgres(k8sAPI *k8s.API, customerConfig *config.Customer, namespace config.Namespace) error {
	if namespace.Postgres {
		log.Printf("Configure Postgres in namespace %s", namespace.Name)
		if err := installPostgres(k8sAPI, customerConfig, namespace); err != nil {
			return err
		}
	} else {
		log.Printf("No Postgres in namespace %s", namespace.Name)
		if err := removePostgres(k8sAPI, customerConfig, namespace); err != nil {
			return err
		}
	}
	return nil
}

// createPGOClient initializes a new PGO Client
func createPGOClient(k8sAPI *k8s.API) (*Client, error) {

	// get the PGO configmap
	listOptions := metav1.ListOptions{FieldSelector: "metadata.name=pgo-deployer-cm"}
	configMapList, err := k8sAPI.Clientset.CoreV1().ConfigMaps("").List(context.TODO(), listOptions)
	if err != nil {
		return nil, errors.Errorf("Could not find configmap 'pgo-deployer-cm' in any namespace; err %v", err)
	}

	// parse values.yaml from the configmap
	configMap := configMapList.Items[0]
	values := make(map[string]string)
	err = yaml.Unmarshal([]byte(configMap.Data["values.yaml"]), &values)
	if err != nil {
		return nil, err
	}

	// get the ingress for the url for the PGO API
	pgoNamespace := configMap.ObjectMeta.Namespace
	ingress, err := k8sAPI.Clientset.NetworkingV1().Ingresses(pgoNamespace).Get(context.TODO(), "postgres-operator", metav1.GetOptions{})
	if err != nil {
		return nil, errors.Errorf("Could not find ingress 'postgres-operator', error %v", err)
	}

	// get secret data for user and password of PGO
	secretData, err := k8sAPI.GetSecretData("pgo-admin-secret", pgoNamespace)
	if err != nil {
		return nil, err
	}

	pgoClient := &Client{
		FQDN:                ingress.Spec.Rules[0].Host,
		pgoNamespace:        configMap.ObjectMeta.Namespace,
		pgoInstallationName: values["pgo_installation_name"],
		clientVersion:       values["pgo_client_version"],
		Username:            string(secretData["username"]),
		Password:            string(secretData["password"]),
	}

	return pgoClient, nil
}

// doJSONRequest performs a REST request with the http library
func (pgoClient *Client) doJSONRequest(method string, requestPath string, body interface{}, response interface{}, allowedStatuscode int) error {

	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(body)

	url := fmt.Sprintf("https://%s/%s", pgoClient.FQDN, requestPath)
	req, err := http.NewRequest(method, url, buf)
	if err != nil {
		return err
	}
	req.SetBasicAuth(pgoClient.Username, pgoClient.Password)
	req.Header.Set("Content-Type", jsonContentType)

	var client = &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	//log.Printf("URL: %s, Method %s, Body: %s", url, method, buf)
	if res.StatusCode != allowedStatuscode {
		json, _ := json.Marshal(body)
		return errors.Errorf("URL: %s, Method %s, Body: %s, Error status code: %d", url, method, json, res.StatusCode)
	}

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	//log.Printf("URL: %s, Method %s, Response: %s", url, method, responseData)
	if len(responseData) > 0 && response != nil {
		err = json.Unmarshal(responseData, response)
		if err != nil {
			return errors.Errorf("URL: %s, Method %s, Body: %s, Response: %s", url, method, body, responseData)
		}
	}
	return nil
}

// Client struct for PGO Rest API
type Client struct {
	FQDN                string
	Username            string
	Password            string
	clientVersion       string
	pgoNamespace        string
	pgoInstallationName string
}

// serviceAccounts is an array of all the service accounts created by Crunchy per namespace
var serviceAccounts = []string{"pgo-backrest", "pgo-default", "pgo-pg", "pgo-target"}

// installPostgres installs the RBAC for the Postgres operator in the namespace
func installPostgres(k8sAPI *k8s.API, customerConfig *config.Customer, namespace config.Namespace) error {

	pgoClient, err := createPGOClient(k8sAPI)
	if err != nil {
		return err
	}
	if err := pgoClient.installOperatorRBAC(k8sAPI, namespace); err != nil {
		return err
	}
	if err := pgoClient.configureSecurityRoleBindings(k8sAPI, namespace); err != nil {
		return err
	}

	pgoUserAdmin := localPGOAdmin(namespace)

	// Delete the user from pgo in case the secret with the admin password was lost
	secretExists, err := k8sAPI.SecretExists("pgo-user-admin", namespace.Name)
	if err != nil {
		return err
	}
	if !secretExists {
		if err := pgoClient.deleteLocalPGOAdminUser(pgoUserAdmin, namespace); err != nil {
			return err
		}
	}

	// Create the local PGO admin user if it does not exist yet
	if !pgoClient.getLocalPGOAdminUser(pgoUserAdmin) {
		if err := pgoClient.createLocalPGOAdminUser(k8sAPI, pgoUserAdmin, namespace); err != nil {
			return err
		}
	}
	return nil
}

// localPGOAdmin gets the name of the local PGO admin
func localPGOAdmin(namespace config.Namespace) string {
	return fmt.Sprintf("%s-pgo-user-admin", namespace.Name)
}

// Check if the namespace is managed by the PGO operator
func isManagedByPGOOperator(ns *corev1.Namespace) bool {
	for key := range ns.ObjectMeta.Labels {
		if key == "pgo-installation-name" {
			return true
		}
	}
	return false
}

func (pgoClient Client) getPGOClusterRole(k8sAPI *k8s.API) (string, error) {
	clusterRoles, _ := k8sAPI.Clientset.RbacV1().ClusterRoles().List(context.TODO(), metav1.ListOptions{})
	for _, clusterRole := range clusterRoles.Items {
		if strings.HasSuffix(clusterRole.Name, "pgo-serviceaccount") {
			return clusterRole.Name, nil
		}
	}
	return "", errors.Errorf("Could not find clusterrole for PGO service account")
}

// configureSecurityRoleBindings creates a rolebinding which allows the PGO PSP
// See also https://github.com/CrunchyData/postgres-operator/issues/1362
// The PGO operator role installs a PSP which is used below.
func (pgoClient Client) configureSecurityRoleBindings(k8sAPI *k8s.API, namespace config.Namespace) error {

	for _, serviceAccount := range serviceAccounts {
		pgoClusterRoleName, err := pgoClient.getPGOClusterRole(k8sAPI)
		if err != nil {
			return err
		}
		rolebinding := rbacv1.RoleBinding{
			ObjectMeta: metav1.ObjectMeta{
				Name: serviceAccount + "-security-postgres-sa",
				Labels: map[string]string{
					"vendor":       "crunchydata",
					"cno-addition": "true",
				},
			},
			RoleRef: rbacv1.RoleRef{
				Name:     pgoClusterRoleName,
				APIGroup: rbacv1.GroupName,
				Kind:     "ClusterRole",
			},
			Subjects: []rbacv1.Subject{
				{
					Name:      serviceAccount,
					Kind:      "ServiceAccount",
					Namespace: namespace.Name,
				},
			},
		}
		if _, err := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Get(context.TODO(), rolebinding.Name, metav1.GetOptions{}); err != nil {
			log.Printf("Create rolebinding for security ClusterRole (PSP/SCC) to serviceaccount %s", serviceAccount)
			if _, err := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Create(context.TODO(), &rolebinding, metav1.CreateOptions{}); err != nil {
				return err
			}
		}
	}
	return nil
}

// installOperatorRBAC installs roles and service account for the operator
// see 'readonly & disabled Namespace Operating Modes' in https://access.crunchydata.com/documentation/postgres-operator/latest/architecture/namespace/
func (pgoClient Client) installOperatorRBAC(k8sAPI *k8s.API, namespace config.Namespace) error {
	localNamespaceRBAC, err := pgoClient.fetchLocalNamespaceRBAC()
	if err != nil {
		return err
	}
	localNamespaceRBAC = strings.ReplaceAll(localNamespaceRBAC, "$PGO_OPERATOR_NAMESPACE", pgoClient.pgoNamespace)
	if err := k8sAPI.ApplyManifests(namespace.Name, localNamespaceRBAC); err != nil {
		return err
	}
	return nil
}

// fetchLocalNamespaceRBAC gets the file with the RBAC manifests from github
func (pgoClient Client) fetchLocalNamespaceRBAC() (string, error) {
	url := fmt.Sprintf("https://raw.githubusercontent.com/CrunchyData/postgres-operator/v%s/deploy/local-namespace-rbac.yaml", pgoClient.clientVersion)
	// Get the data
	res, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	responseData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	return string(responseData), nil
}

// createLocalPGOAdminUser creates a local PGO admin user
func (pgoClient Client) createLocalPGOAdminUser(k8sAPI *k8s.API, username string, namespace config.Namespace) error {

	password, err := password.Generate(20, 5, 5, false, false)
	if err != nil {
		return err
	}

	type UserDefinition struct {
		ClientVersion     string
		PgouserName       string
		PgouserPassword   string
		PgouserRoles      string
		AllNamespaces     bool
		PgouserNamespaces string
		Namespace         string
	}

	userDefinition := UserDefinition{
		ClientVersion:     pgoClient.clientVersion,
		PgouserName:       username,
		PgouserPassword:   password,
		PgouserRoles:      "pgoadmin",
		AllNamespaces:     false,
		PgouserNamespaces: namespace.Name,
		Namespace:         namespace.Name,
	}

	log.Printf("Create PGO Admin user %s in namespace %s", username, namespace.Name)
	if err := pgoClient.doJSONRequest(http.MethodPost, "pgousercreate", &userDefinition, nil, 200); err != nil {
		return err
	}

	secret := corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name: "pgo-user-admin",
			Labels: map[string]string{
				"vendor":       "crunchydata",
				"cno-addition": "true",
			},
		},
		Data: map[string][]byte{
			"username": []byte(username),
			"password": []byte(password),
		},
	}

	log.Printf("Create secret pgo-user-admin for PGO local admin %s in namespace %s", username, namespace.Name)
	if _, err := k8sAPI.Clientset.CoreV1().Secrets(namespace.Name).Create(context.TODO(), &secret, metav1.CreateOptions{}); err != nil {
		return err
	}

	return nil
}

// getLocalPGOAdminUser checks if the PGO admin user exists
func (pgoClient *Client) getLocalPGOAdminUser(username string) bool {
	type ShowUserRequest struct {
		ClientVersion string
		PgouserName   []string
	}

	showUserRequest := ShowUserRequest{
		ClientVersion: pgoClient.clientVersion,
		PgouserName:   []string{username},
	}

	type UserInfo struct {
		Role []string
	}
	type ShowUserResponse struct {
		UserInfo []UserInfo
	}
	var response ShowUserResponse
	pgoClient.doJSONRequest(http.MethodPost, "pgousershow", &showUserRequest, &response, 200)

	return len(response.UserInfo) > 0 && len(response.UserInfo[0].Role) > 0
}

// removePostgres removes Postgres Operator artefacts from namespace
func removePostgres(k8sAPI *k8s.API, customerConfig *config.Customer, namespace config.Namespace) error {
	pgoClient, err := createPGOClient(k8sAPI)
	if err != nil {
		return err
	}

	if pgoClient.getLocalPGOAdminUser(localPGOAdmin(namespace)) {
		if err := pgoClient.deleteLocalPGOAdminUser(localPGOAdmin(namespace), namespace); err != nil {
			return err
		}
	}
	if err := pgoClient.removeOperatorRBAC(k8sAPI, namespace); err != nil {
		return err
	}
	if err := pgoClient.removePGOSecrets(k8sAPI, namespace); err != nil {
		return err
	}
	return nil
}

// removeOperatorRBAC removes the PGO RBAC from the namespace
func (pgoClient Client) removeOperatorRBAC(k8sAPI *k8s.API, namespace config.Namespace) error {
	options := metav1.ListOptions{
		LabelSelector: "vendor=crunchydata",
	}
	roleList, _ := k8sAPI.Clientset.RbacV1().Roles(namespace.Name).List(context.TODO(), options)
	for _, role := range roleList.Items {
		log.Printf("Delete role %s from namespace %s", role.Name, namespace.Name)
		err := k8sAPI.Clientset.RbacV1().Roles(namespace.Name).Delete(context.TODO(), role.Name, metav1.DeleteOptions{})
		if err != nil {
			return err
		}
	}
	roleBindings, _ := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).List(context.TODO(), options)
	for _, roleBinding := range roleBindings.Items {
		log.Printf("Delete rolebinding %s from namespace %s", roleBinding.Name, namespace.Name)
		err := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Delete(context.TODO(), roleBinding.Name, metav1.DeleteOptions{})
		if err != nil {
			return err
		}
	}
	serviceAccounts, _ := k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).List(context.TODO(), options)
	for _, serviceAccount := range serviceAccounts.Items {
		log.Printf("Delete serviceAccount %s from namespace %s", serviceAccount.Name, namespace.Name)
		err := k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).Delete(context.TODO(), serviceAccount.Name, metav1.DeleteOptions{})
		if err != nil {
			return err
		}
	}

	// Some objects lack the vendor=crunchydata label (see https://github.com/CrunchyData/postgres-operator/issues/2470)
	// So, until that issue is fixed, delete the objects one by one :( (once fixed, the lines until 'return nil' can be removed)
	unlabeledRoles := []string{"pgo-backrest-role", "pgo-target-role", "pgo-local-ns"}
	for _, role := range unlabeledRoles {
		if _, err := k8sAPI.Clientset.RbacV1().Roles(namespace.Name).Get(context.TODO(), role, metav1.GetOptions{}); err != nil {
			// Role not present, continue
		} else {
			log.Printf("Delete role %s from namespace %s", role, namespace.Name)
			if err := k8sAPI.Clientset.RbacV1().Roles(namespace.Name).Delete(context.TODO(), role, metav1.DeleteOptions{}); err != nil {
				return err
			}
		}
	}
	unlabeledRoleBindings := []string{"pgo-backrest-role-binding", "pgo-target-role-binding", "pgo-local-ns"}
	for _, roleBinding := range unlabeledRoleBindings {
		if _, err := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Get(context.TODO(), roleBinding, metav1.GetOptions{}); err != nil {
			// Role binding not present, continue
		} else {
			log.Printf("Delete role binding %s from namespace %s", roleBinding, namespace.Name)
			if err := k8sAPI.Clientset.RbacV1().RoleBindings(namespace.Name).Delete(context.TODO(), roleBinding, metav1.DeleteOptions{}); err != nil {
				return err
			}
		}
	}
	unlabeledServiceAccounts := []string{"pgo-backrest", "pgo-default", "pgo-target"}
	for _, serviceAccount := range unlabeledServiceAccounts {
		if _, err := k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).Get(context.TODO(), serviceAccount, metav1.GetOptions{}); err != nil {
			// Service account not present, continue
		} else {
			log.Printf("Delete service account %s from namespace %s", serviceAccount, namespace.Name)
			if err := k8sAPI.Clientset.CoreV1().ServiceAccounts(namespace.Name).Delete(context.TODO(), serviceAccount, metav1.DeleteOptions{}); err != nil {
				return err
			}
		}
	}

	return nil
}

// removePGOSecrets removes the secrets created by PGO from the namespace
// Secrets created for database clusters (e.g. hippo-hippo-secret, hippo-pgbouncer-secret etc) are _not_ removed.
// Secrets created for SA's are removed with the SA's. So that leaves postgres-operator-dockercfg-* and pgo-user-admin.
func (pgoClient Client) removePGOSecrets(k8sAPI *k8s.API, namespace config.Namespace) error {
	secretList, _ := k8sAPI.Clientset.CoreV1().Secrets(namespace.Name).List(context.TODO(), metav1.ListOptions{})
	for _, secret := range secretList.Items {
		if strings.HasPrefix(secret.Name, "postgres-operator-dockercfg-") || secret.Name == "pgo-user-admin" {
			log.Printf("Delete secret %s from namespace %s", secret.Name, namespace.Name)
			err := k8sAPI.Clientset.CoreV1().Secrets(namespace.Name).Delete(context.TODO(), secret.Name, metav1.DeleteOptions{})
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// deleteLocalPGOAdminUser deletes the PGO admin user
func (pgoClient *Client) deleteLocalPGOAdminUser(username string, namespace config.Namespace) error {
	type DeleteUserRequest struct {
		ClientVersion string
		PgouserName   []string
	}

	deleteUserRequest := DeleteUserRequest{
		ClientVersion: pgoClient.clientVersion,
		PgouserName:   []string{username},
	}

	log.Printf("Delete PGO admin user %s from namespace %s", username, namespace.Name)
	if err := pgoClient.doJSONRequest(http.MethodPost, "pgouserdelete", &deleteUserRequest, nil, 200); err != nil {
		return err
	}
	return nil
}
