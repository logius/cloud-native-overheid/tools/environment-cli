package environment

import (
	"log"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/gitlab-cli/gitlabclient"

	"github.com/xanzy/go-gitlab"

	"context"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// RegistriesStruct is a map of registries to their credentials
type RegistriesStruct map[string]RegistryCredentials

// RegistryCredentials defines the fields stored per registry in an docker config secret
type RegistryCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Auth     string `json:"auth"`
}

type HarborPullerParams struct {
	harborPullerName string
	DockerAuthConfigJson []byte
}

// Get gitlab group variable by key, return value as string
func GetGitlabGroupVariable(k8sAPI *k8s.API, gitlabClient *gitlab.Client, groupPath, variableKey string) (string, error) {

	// Lookup robot account from user gitlab group
	gitlabGroup := gitlabclient.GetGitLabGroup(gitlabClient, groupPath)

	// Get gitlab group variable
	value, _, err := gitlabClient.GroupVariables.GetVariable(gitlabGroup.ID, variableKey)
	if err != nil {
		return "", err
	}
	return value.Value, nil
}

// SaveHarborPuller create a harbor-puller secret in the namespace
func SaveHarborPuller(k8sAPI *k8s.API, gitlabClient *gitlab.Client, customerConfig *config.Customer, environment config.Environment, namespace config.Namespace) error {

	for _, harborPuller := range environment.HarborPullers {
		params := HarborPullerParams {
			harborPullerName: harborPuller.Name,
		}

		// Read the DOCKER_AUTH_CONFIG json from the GitLab variable in the docker_auth_config_group
		dockerauthconfig, err := GetGitlabGroupVariable(k8sAPI, gitlabClient, harborPuller.DockerAuthConfigGroupPath, "DOCKER_AUTH_CONFIG")
		if err != nil {
			return err
		}

		params.DockerAuthConfigJson = []byte(dockerauthconfig)

		// Create a secret object
		secret, err := makeSecret(namespace.Name, params)
		if err != nil {
			return err
		}

		// Store secret object in customer namespace
		if _, err := k8sAPI.Clientset.CoreV1().Secrets(namespace.Name).Get(context.TODO(), secret.Name, metav1.GetOptions{}); err != nil {
			log.Printf("Create Secrets %v in namespace %s using DOCKER_AUTH_CONFIG", secret.Name, namespace.Name)
			_, err = k8sAPI.Clientset.CoreV1().Secrets(namespace.Name).Create(context.TODO(), secret, metav1.CreateOptions{})
			if err != nil {
				return err
			}
		} else {
			log.Printf("Update Secrets %v in namespace %s using DOCKER_AUTH_CONFIG", secret.Name, namespace.Name)
			_, err = k8sAPI.Clientset.CoreV1().Secrets(namespace.Name).Update(context.TODO(), secret, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// Make a secret object where dockerconfigjson contains:
// {"auths":{"myharbor.localhost":{"username":"robot$username","password":"**","email":"","auth":"**"}}}
func makeSecret(namespace string, params HarborPullerParams) (*corev1.Secret, error) {

	return &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      params.harborPullerName,
			Namespace: namespace,
		},
		Type: "kubernetes.io/dockerconfigjson",
		Data: map[string][]byte{
			".dockerconfigjson": params.DockerAuthConfigJson,
		},
	}, nil
}
