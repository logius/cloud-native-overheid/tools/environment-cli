package environment

import (
	"context"
	"log"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"

	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// manageCustomerClusterRole creates or updates customer's clusterrole for impersonation
func manageCustomerClusterRole(k8sAPI *k8s.API, customerConfig *config.Customer, environment config.Environment, groupPrefix string) error {
	impersonator := environment.ImpersonatorClusterRole
	if impersonator.Group != "" && impersonator.User != "" {
		type ClusterRoleParams struct {
			CustomerName    string
			GroupName       string
			ImpersonateUser string
			GroupPrefix     string
		}

		params := ClusterRoleParams{
			CustomerName:    customerConfig.Name,
			GroupName:       impersonator.Group,
			ImpersonateUser: impersonator.User,
			GroupPrefix:     groupPrefix,
		}

		// Impersonator clusterrole
		var clusterRole rbacv1.ClusterRole
		clusterRoleFileName := "deployments/impersonator/impersonator-clusterrole.yml"
		if err := parseDeploymentFile(clusterRoleFileName, params, &clusterRole); err != nil {
			return err
		}
		clusterRole.ObjectMeta.Labels = commonLabels(clusterRole.ObjectMeta.Labels)

		if _, err := k8sAPI.Clientset.RbacV1().ClusterRoles().Get(context.TODO(), clusterRole.Name, metav1.GetOptions{}); err != nil {
			log.Printf("Create clusterrole %s", clusterRole.Name)
			_, err = k8sAPI.Clientset.RbacV1().ClusterRoles().Create(context.TODO(), &clusterRole, metav1.CreateOptions{})
			if err != nil {
				return err
			}
		} else {
			log.Printf("Update clusterrole %s", clusterRole.Name)
			_, err = k8sAPI.Clientset.RbacV1().ClusterRoles().Update(context.TODO(), &clusterRole, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
		}
		// Clusterrolebindings for impersonator clusterrole
		clusterRolebindingFileName := "deployments/impersonator/impersonator-clusterrolebinding.yml"
		var clusterRoleBinding rbacv1.ClusterRoleBinding
		if err := parseDeploymentFile(clusterRolebindingFileName, params, &clusterRoleBinding); err != nil {
			return err
		}

		if _, err := k8sAPI.Clientset.RbacV1().ClusterRoleBindings().Get(context.TODO(), clusterRoleBinding.Name, metav1.GetOptions{}); err != nil {
			log.Printf("Create clusterrolebinding %s for binding clusterrole to %s", clusterRoleBinding.Name, impersonator.Group)
			_, err = k8sAPI.Clientset.RbacV1().ClusterRoleBindings().Create(context.TODO(), &clusterRoleBinding, metav1.CreateOptions{})
			if err != nil {
				return err
			}
		} else {
			// Update of clusterrole in the clusterrolebinding is not (always) possible, so try and if not possible, delete and recreate
			log.Printf("Update clusterrolebinding %s for binding clusterrole to %s", clusterRoleBinding.Name, impersonator.Group)
			_, err = k8sAPI.Clientset.RbacV1().ClusterRoleBindings().Update(context.TODO(), &clusterRoleBinding, metav1.UpdateOptions{})
			if err != nil {

				log.Printf("Update not possible, so re-create clusterrolebinding %s for binding clusterrole to %s", clusterRoleBinding.Name, impersonator.Group)
				err = k8sAPI.Clientset.RbacV1().ClusterRoleBindings().Delete(context.TODO(), clusterRoleBinding.Name, metav1.DeleteOptions{})
				if err != nil {
					return err
				}
				_, err = k8sAPI.Clientset.RbacV1().ClusterRoleBindings().Create(context.TODO(), &clusterRoleBinding, metav1.CreateOptions{})
				if err != nil {
					return err
				}

			}
		}

	} else {
		log.Printf("No impersonator role for customer %s", customerConfig.Name)
	}

	return nil
}
