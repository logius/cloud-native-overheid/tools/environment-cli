package environment

import (
	"context"
	"log"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ManageNamespace creates or updates a namespace
func ManageNamespace(k8sAPI *k8s.API, customerConfig *config.Customer, environment config.Environment, namespace config.Namespace, skipPostgresPart bool) error {

	adminNamespace := "false"
	if environment.AdminNamespace == namespace.Name {
		adminNamespace = "true"
	}
	labels := map[string]string{
		"marked-for-deletion": "false",
		"customer":            customerConfig.Name,
		"admin_namespace":     adminNamespace,
	}
	if !skipPostgresPart {
		if namespace.Postgres {
			// Set labels for the PGO operator, needed to let the operator watch this namepace.
			pgoClient, err := createPGOClient(k8sAPI)
			if err != nil {
				return err
			}
			labels["pgo-created-by"] = "pgouser-admin"
			labels["pgo-installation-name"] = pgoClient.pgoInstallationName
			labels["vendor"] = "crunchydata"
		}
	}
	ns := corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   namespace.Name,
			Labels: labels,
		},
	}

	if foundNamespace, err := k8sAPI.Clientset.CoreV1().Namespaces().Get(context.TODO(), ns.Name, metav1.GetOptions{}); err != nil {
		log.Printf("Create namespace %s", ns.Name)
		_, err = k8sAPI.Clientset.CoreV1().Namespaces().Create(context.TODO(), &ns, metav1.CreateOptions{})
		if err != nil {
			return err
		}
	} else {
		// Using the 'ns' object resulted in unwanted changes in openshift.io/sa.scc.* annotations on an Openshift cluster.
		// Set labels on the found namespace and use this object to update.
		for k, v := range foundNamespace.Labels {
			foundNamespace.Labels[k] = v
		}
		_, err = k8sAPI.Clientset.CoreV1().Namespaces().Update(context.TODO(), foundNamespace, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
	}
	return nil
}
