package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/cluster"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/environment"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "environment",
		Short: "OPS tools for K8s environments",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(environment.NewCommand())
	cmd.AddCommand(cluster.NewCommand())
}
