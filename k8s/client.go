package k8s

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	yaml "sigs.k8s.io/yaml"
)

// API holds two K8s client interfaces: clientset and dynamic
type API struct {
	Cluster   string
	Clientset *kubernetes.Clientset
	Dynamic   dynamic.Interface
	Config    *rest.Config
}

// CreateAPI initializes a K8s clientset and a dynamic K8s client
func CreateAPI(context string) (*API, error) {

	config := getConfig(context)

	clientConfig, err := config.ClientConfig()
	if err != nil {
		return nil, err
	}

	if context == "" {
		raw, _ := config.RawConfig()
		context = raw.CurrentContext
	}

	log.Printf("K8s context %q", context)

	clientSet, err := createClientset(clientConfig)
	if err != nil {
		return nil, err
	}
	dynamicClient, err := createDynamicClient(clientConfig)
	if err != nil {
		return nil, err
	}

	return &API{
		Cluster:   context,
		Config:    clientConfig,
		Clientset: clientSet,
		Dynamic:   dynamicClient,
	}, nil
}

// getConfig returns a Kubernetes client config for a given context.
func getConfig(context string) clientcmd.ClientConfig {

	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	configOverrides := &clientcmd.ConfigOverrides{}
	if context != "" {
		configOverrides.CurrentContext = context
	}
	return clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, configOverrides)
}

// createClientset creates the K8s Client
func createClientset(config *rest.Config) (*kubernetes.Clientset, error) {

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return clientset, nil
}

// CreateDynamicClient creates dynamic K8s Client
func createDynamicClient(config *rest.Config) (dynamic.Interface, error) {

	restClient, err := dynamic.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return restClient, nil
}

// ApplyManifests applies a set of manifests
func (k8sAPI API) ApplyManifests(namespace string, manifests string) error {
	for _, manifest := range strings.Split(manifests, "---") {
		if manifest == "" {
			continue
		}

		// unmarshall the yaml manifest to an unstructured object
		var object unstructured.Unstructured
		err := yaml.Unmarshal([]byte(manifest), &object)
		if err != nil {
			return err
		}

		// get the Group, Version and Resource from the object
		gvk := object.GroupVersionKind()
		gvr := schema.GroupVersionResource{
			Group:    gvk.Group,
			Version:  gvk.Version,
			Resource: strings.ToLower(gvk.Kind + "s"), // Not sure about this, but singular does not work
		}

		// Apply the object with create or update
		if _, err := k8sAPI.Dynamic.Resource(gvr).Namespace(namespace).Get(context.TODO(), object.GetName(), metav1.GetOptions{}); err != nil {
			log.Printf("Create %s %s in namespace %s", object.GetKind(), object.GetName(), namespace)
			_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(namespace).Create(context.TODO(), &object, metav1.CreateOptions{})
			if err != nil {
				return err
			}
		} else {
			log.Printf("Update %s %s in namespace %s", object.GetKind(), object.GetName(), namespace)
			_, err = k8sAPI.Dynamic.Resource(gvr).Namespace(namespace).Update(context.TODO(), &object, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// SecretExists checks if a secret exists
func (k8sAPI API) SecretExists(secretName string, namespace string) (bool, error) {
	_, err := k8sAPI.Clientset.CoreV1().Secrets(namespace).Get(context.TODO(), secretName, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

// GetSecretData gets the data for a secret
func (k8sAPI API) GetSecretData(secretName string, namespace string) (map[string][]byte, error) {

	secret, err := k8sAPI.Clientset.CoreV1().Secrets(namespace).Get(context.TODO(), secretName, metav1.GetOptions{})
	if errors.IsNotFound(err) {
		fmt.Printf("Secret %s in namespace %s not found\n", secretName, namespace)
	} else if statusError, isStatus := err.(*errors.StatusError); isStatus {
		fmt.Printf("Error getting secret %s in namespace %s: %v\n",
			secretName, namespace, statusError.ErrStatus.Message)
	} else if err != nil {
		return nil, err
	}

	return secret.Data, nil
}

// patchStringValue specifies the patch operations
type patchStringValue struct {
	Op    string `json:"op"`
	Path  string `json:"path"`
	Value string `json:"value"`
}

func (k8sAPI API) PatchNamespaceLabels(namespace string, labelPatches map[string]string, operation string) error {

	patches := make([]patchStringValue, 0)
	for key, value := range labelPatches {
		patches = append(patches, patchStringValue{
			Op:    operation,
			Path:  "/metadata/labels/" + key,
			Value: value,
		})
	}
	patchBytes, _ := json.Marshal(patches)

	log.Printf("Configure labels for namespace %s", namespace)
	_, err := k8sAPI.Clientset.CoreV1().Namespaces().Patch(context.TODO(), namespace, types.JSONPatchType, patchBytes, metav1.PatchOptions{})
	return err
}
