package config

import (
	"fmt"
)

// ValidateConfig valiudates the configuration
func (cfg *Config) ValidateConfig() []error {

	var errorList []error

	for _, environment := range cfg.Customer.Environments {
		for _, ns := range environment.Namespaces {
			for _, rolebinding := range ns.Rolebindings {
				if rolebinding.Group != "" {
					if !cfg.isGroupDefined(rolebinding.Group) {
						err := fmt.Errorf("Unknown group %q in namespace %q", rolebinding.Group, ns.Name)
						errorList = append(errorList, err)
					}
				} else if len(rolebinding.Groups) > 0 {
					for _, group := range rolebinding.Groups {
						if !cfg.isGroupDefined(group) {
							err := fmt.Errorf("Unknown group %q in namespace %q", group, ns.Name)
							errorList = append(errorList, err)
						}
					}
				} else {
					err := fmt.Errorf("Groups should be defined in rolebinding %q in namespace %q", rolebinding.ClusterRole, ns.Name)
					errorList = append(errorList, err)
				}
			}
		}
	}
	return errorList
}

func (cfg *Config) isGroupDefined(groupName string) bool {
	for _, group := range cfg.Customer.Groups {
		if group.Name == groupName {
			return true
		}
	}
	return false
}
