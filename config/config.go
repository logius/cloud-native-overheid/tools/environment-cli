package config

import (
	"io/ioutil"
	"log"

	"github.com/pkg/errors"

	"gopkg.in/yaml.v3"
)

// Rolebinding binds a K8s role to an IAM group.
type Rolebinding struct {
	ClusterRole string `yaml:"clusterrole"`
	Group       string
	Groups      []string `yaml:"groups,omitempty"`
	Kind        string
}

// Impersonate Rolebindings binds a K8s role to the impersonator user.
type ImpersonateRolebinding struct {
	ClusterRole string `yaml:"clusterrole"`
	User        string
	Users       []string `yaml:"users"`
	Kind        string
}

// ServiceAccount creates a serviceaccount and binds it to a clusterrole.
type ServiceAccount struct {
	Name        string
	ClusterRole string `yaml:"clusterrole"`
}

// ResourceQuota defines the requests and limits on resources
type ResourceQuota struct {
	CPULimits       string `yaml:"limits.cpu,omitempty"`
	MemoryLimits    string `yaml:"limits.memory,omitempty"`
	CPURequests     string `yaml:"requests.cpu,omitempty"`
	MemoryRequests  string `yaml:"requests.memory,omitempty"`
	StorageRequests string `yaml:"requests.storage,omitempty"`
}

// Namespace defines the K8s namespace for a customer.
type Namespace struct {
	Name                          string
	Rolebindings                  []Rolebinding            `yaml:"rolebindings"`
	ImpersonateRolebindings       []ImpersonateRolebinding `yaml:"impersonate_rolebindings,omitempty"`
	ServiceAccounts               []ServiceAccount         `yaml:"serviceaccounts,omitempty"`
	ResourceQuota                 ResourceQuota            `yaml:"resourcequota,omitempty"`
	NoResourceQuota               bool                     `yaml:"no_resourcequota,omitempty"`
	DisableDefaultNetworkPolicies bool                     `yaml:"disable_default_networkpolicies,omitempty"`
	Postgres                      bool
	CustomWildcardDNSName         string `yaml:"custom_wildcard_dns_name,omitempty"`
	SkipCertificates              bool   `yaml:"skip_certificates,omitempty"`
}

// HarborPuller defines puller secret.
type HarborPuller struct {
	Name                      string
	DockerAuthConfigGroupPath string `yaml:"docker_auth_config_group_path"`
}

type Impersonator struct {
	User  string `yaml:"user,omitempty"`
	Group string `yaml:"group,omitempty"`
}

// Environment configuration
type Environment struct {
	Stage                   string
	AdminNamespace          string         `yaml:"admin_namespace,omitempty"`
	DeployerSaClusterRole   string         `yaml:"deployer_sa_clusterrole,omitempty"`
	ImpersonatorClusterRole Impersonator   `yaml:"impersonate_clusterrole,omitempty"`
	HarborPullers           []HarborPuller `yaml:"harbor_pullers,omitempty"`
	Namespaces              []Namespace
}

// Group configuration
type Group struct {
	Name string
}

// GitLabGroupConfig spec
type GitLabGroupConfig struct {
	Name string
}

// GitLab configuration
type GitLab struct {
	Groups []GitLabGroupConfig
}

// Customer configuration
type Customer struct {
	Name         string
	Groups       []Group
	Environments []Environment
	GitLab       GitLab
}

// Config contains customer configuration
type Config struct {
	Customer Customer
}

// LoadConfigfile loads the customer configuration from file
func LoadConfigfile(configFile string) (*Customer, error) {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, errors.Errorf("Error reading YAML file: %s\n", err)
	}
	return newConfig(string(yamlFile))
}

// NewConfig gets the customer configuration
func newConfig(configData string) (*Customer, error) {

	var cfg Config
	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		return nil, errors.Errorf("Could not parse the config err = %v", err)
	}

	validateErrors := cfg.ValidateConfig()
	if len(validateErrors) > 0 {
		for _, err := range validateErrors {
			log.Printf("%v", err)
		}
		return nil, errors.Errorf("Could not handle errors in tenant configuration")
	}

	return &cfg.Customer, nil
}

// GetEnvironment gets the environment for a given stage.
func (customer *Customer) GetEnvironment(stage string) (*Environment, error) {
	if stage == "" {
		return nil, errors.Errorf("Please specify the stage parameter, e.g. --stage=OT")
	}
	for _, environment := range customer.Environments {
		if environment.Stage == stage {
			return &environment, nil
		}
	}
	return nil, errors.Errorf("Could not find environment %s for customer %s", stage, customer.Name)
}
