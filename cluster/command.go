package cluster

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"
)

type flags struct {
	context *string
	roledir *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			err := configureCluster(cmd, &flags)
			if err != nil {
				return err
			}
			return nil
		},
		Use:   "configure-cluster",
		Short: "Configure K8s cluster resources used by environments",
		Long:  "This command configures cluster scoped resources used by environments, such as cluster roles",
	}

	flags.context = cmd.Flags().String("context", "", "K8s context")
	flags.roledir = cmd.Flags().String("roledir", "", "clusterrole directory path")

	return cmd
}

// configureCluster configures the cluster with objects for use in the environments
func configureCluster(cmd *cobra.Command, flags *flags) error {

	// K8s API clients
	k8sAPI, err := k8s.CreateAPI(*flags.context)
	if err != nil {
		return err
	}

	err = ManageClusterRoles(k8sAPI, *flags.roledir)
	if err != nil {
		return err
	}

	return nil
}
