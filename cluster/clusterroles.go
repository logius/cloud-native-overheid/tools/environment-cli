package cluster

import (
	"bytes"
	"context"
	"html/template"
	"log"
	"path/filepath"

	"gitlab.com/logius/cloud-native-overheid/tools/environment-cli/k8s"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	yaml "sigs.k8s.io/yaml"
)

// ManageClusterRoles provisions cluster roles required for environments
func ManageClusterRoles(k8sAPI *k8s.API, roleDir string) error {

	selectedFiles, _ := filepath.Glob(roleDir + "/*")
	for _, fileName := range selectedFiles {

		var clusterRole rbacv1.ClusterRole
		parseDeploymentFile(fileName, nil, &clusterRole)

		if _, err := k8sAPI.Clientset.RbacV1().ClusterRoles().Get(context.TODO(), clusterRole.Name, metav1.GetOptions{}); err != nil {
			log.Printf("Create clusterrole %s", clusterRole.Name)
			_, err = k8sAPI.Clientset.RbacV1().ClusterRoles().Create(context.TODO(), &clusterRole, metav1.CreateOptions{})
			if err != nil {
				return err
			}
		} else {
			log.Printf("Update clusterrole %s", clusterRole.Name)
			_, err = k8sAPI.Clientset.RbacV1().ClusterRoles().Update(context.TODO(), &clusterRole, metav1.UpdateOptions{})
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func parseDeploymentFile(fileName string, variables interface{}, object interface{}) error {
	tpl, err := template.ParseFiles(fileName)
	if err != nil {
		return err
	}

	var buf bytes.Buffer
	if err := tpl.Execute(&buf, variables); err != nil {
		return err
	}

	if err := yaml.Unmarshal(buf.Bytes(), object); err != nil {
		return err
	}
	return nil
}
