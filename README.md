# Environment-cli
The environment-cli is a command line interface to create a Kubernetes environment for customers of CNO-based Kubernetes clusters and to integrate it with other CNO components such as GitLab and Harbor.

In case integration with other components is configured, make sure that an environment is already created in those components using e.g. gitlab-cli, harbor-cli.

The `configure-environment` command creates:
* One or more namespaces in one or more Kubernetes clusters with specific labels (also enables Crunchy Postgres operator in the namespace when configured)
* Service accounts, secrets, role bindings, network policies, resource quota in the namespaces
* Impersonation
* Kubernetes integration with the clusters configured in GitLab group
* Harbor integration with the customer's project(s) configured in a `dockercfg` secret in the namespaces

The `configure-cluster` command creates:
* A set of cluster roles that can later be bound to service accounts and users in the namespaces

## Authorization

### GitLab to Kubernetes
The environment-cli creates a service account `deployer` which is bound to a cluster role that is defined in the configuration and must be created on beforehand with the `configure-cluster` command.

### Kubernetes to Harbor
The environment-cli creates a secret named `harbor-puller` with credentials for the account that can be found in GitLab variables as defined in the configuration.

### Users in Kubernetes
The environment-cli creates role bindings between LDAP groups and cluster roles in the namespaces, as specified in the configuration. The cluster roles must be created on beforehand with the `configure-cluster` command.

### Postgres Operator
The environment-cli creates a number of service accounts for use by the Postgres Operator, plus role bindings. It also creates a secret `pgo-user-admin` with credentials that can be used for the PGO API for actions in that namespace.

### Impersonator Clusterrole
The environment-cli creates a clusterrole and clusterrolebinding for impersonation, plus a rolebinding per namespace where the impersonation is needed. To enable the impersonation clusterrole please use the following config:
```yaml
environments:
  - stage: OT
    impersonate_clusterrole: 
      user: sample_user
      group: sample_group
    namespaces: 
      - name: sample-ontwikkel
        impersonate_rolebindings:
          - clusterrole: tenant-readwrite
            user: sample-user
```

## Feature add harbor-puller secret in customer namespace

This feature creates harbor puller secrets in the customer namespaces.
To enable this feature add section harbor_pullers at the environment section.
```yaml
  environments:
    - stage: OT
      harbor_pullers:
        - name: harbor-puller
          registry: harbor.localhost
          gitlab_variable:
            group_path: customer
            username_key: CUSTOMER_HARBOR_ROBOT_NAME
            password_key: CUSTOMER_HARBOR_ROBOT_SECRET
```

The harbor robot credentials will be looked up from gitlab customer group variables with name CUSTOMER_HARBOR_ROBOT_NAME and CUSTOMER_HARBOR_ROBOT_SECRET.
